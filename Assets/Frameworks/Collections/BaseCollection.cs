﻿using System.Linq;
using System.Collections.Generic;
namespace SFramework.Collections
{
    public abstract class BaseCollection<T>
    {
        protected Dictionary<int, T> collection;

        public BaseCollection()
        {
            collection = new Dictionary<int, T>();
        }

        internal virtual void Add(int key, T value)
        {
            if (collection.ContainsKey(key))
            {
                collection[key] = value;
            }
            else
            {
                collection.Add(key, value);
            }
        }

        internal virtual void Remove(int key)
        {
            if (collection.ContainsKey(key))
            {
                collection.Remove(key);
            }
            else
            {
                throw new KeyNotFoundException(key.ToString());
            }
        }

        internal virtual T Get(int key)
        {
            T item = default(T);
            collection.TryGetValue(key, out item);
            return item;
        }

        internal int Count
        {
            get
            {
                return collection == null ? 0 : collection.Count;
            }
        }

        internal virtual T[] GetAll()
        {
            return collection.Values.ToArray();
        }

        internal abstract void Initialize(List<T> list);
    }
}
