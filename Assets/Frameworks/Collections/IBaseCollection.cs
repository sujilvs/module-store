﻿using System.Collections.Generic;
namespace SFramework.Collections
{
    public interface IBaseCollection<T>
    {
        void Initialize(List<T> list);
        T Get(int key);
        int Count { get; }
        void Add(int key, T value);
        void Remove(int key);
        T[] GetAll();
        void Clear();
    }
}
