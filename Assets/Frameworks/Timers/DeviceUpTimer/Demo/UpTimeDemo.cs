﻿using UnityEngine;
using UnityEngine.UI;

public class UpTimeDemo : MonoBehaviour
{
    [SerializeField] private Text timerText;

    private DeviceUpTimer deviceUpTimer;

    private void Awake()
    {
        deviceUpTimer = new DeviceUpTimer();
    }

    public void OnButtonClick()
    {
        timerText.text = deviceUpTimer.GetUpTime().ToString();
    }
}
