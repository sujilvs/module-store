﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBridge
{
    long GetCurrentUpTime();
}
