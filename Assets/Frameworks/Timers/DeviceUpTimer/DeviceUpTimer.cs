﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceUpTimer
{
    private readonly IBridge bridge;

    public DeviceUpTimer() =>
#if UNITY_IOS && !UNITY_EDITOR
        bridge = new iOSBridge();
#elif UNITY_ANDROID && !UNITY_EDITOR
        bridge = new AndroidBridge();
#elif UNITY_STANDALONE || UNITY_EDITOR
        bridge = new UnityEditorBridge();
#endif

    public double GetUpTime()
    {
        return bridge.GetCurrentUpTime();
    }
}
