﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidBridge : IBridge
{
    AndroidJavaObject jo;
    public AndroidBridge()
    {
        jo = new AndroidJavaObject("android.os.SystemClock");
    }

    public long GetCurrentUpTime()
    {
        long time = jo.CallStatic<long>("elapsedRealtime");
        long timeSec = time / 1000;
        return timeSec;
    }
}
