﻿using System;
using System.Runtime.InteropServices;
public class iOSBridge : IBridge
{
    [DllImport("__Internal")]
    public static extern string _GetCurrentMediaTime();

    public static string GetCurrentMediaTime()
    {
            return _GetCurrentMediaTime();
    }
    public long GetCurrentUpTime()
    {
       return  (long)Convert.ToDouble(GetCurrentMediaTime());
    }
}    