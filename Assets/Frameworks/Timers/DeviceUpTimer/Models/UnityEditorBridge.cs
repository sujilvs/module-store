﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityEditorBridge : IBridge
{
    public long GetCurrentUpTime()
    {
        int time = Environment.TickCount;

        if (time < 0)
            time = Int32.MaxValue + Environment.TickCount;

        int timeSec = time / 1000;
        return (long)timeSec;
    }
}
