﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Auto destroy is used to destroy object after t secs
/// </summary>
public class AutoDestroy : MonoBehaviour
{
    public float destroyTime;

    // Use this for initialization
    void Start()
    {
        Destroy(this.gameObject, destroyTime);
    }
}
