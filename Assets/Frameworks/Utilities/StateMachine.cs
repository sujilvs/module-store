﻿using UnityEngine;
using System.Collections;
using System;
 public class StateMachine
    {
        private int m_currentState = -1;
        private int m_NextState = -1;
        private int m_lastState = -1;

        private bool m_debug = false;

        public delegate void Function();

        Function[] m_onEnterFunc;
        Function[] m_onExitFunc;
        Function[] m_onUpdateFunc;

        float m_time = 0;

        public StateMachine(Enum nStates) : this(Convert.ToInt32(nStates))
        {
        }

        public StateMachine(int nStates, bool debug = false)
        {
            m_onEnterFunc = new Function[nStates];
            m_onExitFunc = new Function[nStates];
            m_onUpdateFunc = new Function[nStates];

            m_debug = debug;
        }

        public void RegisterState(Enum State, Function OnEnter = null, Function OnUpdate = null, Function OnExit = null)
        {
            m_onEnterFunc[Convert.ToInt32(State)] = OnEnter;
            m_onExitFunc[Convert.ToInt32(State)] = OnExit;
            m_onUpdateFunc[Convert.ToInt32(State)] = OnUpdate;
        }

        public void SetState(Enum state)
        {
            int newState = Convert.ToInt32(state);
            m_NextState = newState;
            if (m_debug) Debug.Log("SET From " + m_currentState + " to " + m_NextState);
        }

        public bool IsState(Enum state)
        {
            return m_currentState == Convert.ToInt32(state);
        }
        public int GetState()
        {
            return m_currentState;
        }
        public int GetLastState()
        {
            return m_lastState;
        }
        public float GetStateTime()
        {
            return m_time;
        }

        public void Update()
        {
            m_time += Time.deltaTime;
            if (m_currentState != m_NextState)
            {
                if (m_currentState >= 0)
                {
                    Execute(m_onExitFunc[m_currentState]); //OnExit
                }
                if (m_debug) Debug.Log("From " + m_currentState + " to " + m_NextState);
                m_lastState = m_currentState;
                m_currentState = m_NextState;
                Execute(m_onEnterFunc[m_currentState]); //OnEnter
                m_time = 0;
            }
            Execute(m_onUpdateFunc[m_currentState]); //OnUpdate
        }

        private void Execute(Function f)
        {
            if (f != null)
            {
                f();
            }
        }
    }
